require 'check_wins'
require 'board'
require 'wins'
require 'player/player'
require 'board_snapshot'
3
describe CheckWins do
  let(:board) { Board.new(3) }
  let(:player1) { Player.new }
  let(:player2) { Player.new }
  let(:check_wins) { CheckWins.new(Wins) }
  
  describe ".cats_game?:" do
    it "should not be a cats game when game starts" do
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      expect(check_wins.cats_game?(board_snapshot) ).to be false
    end

    it "should not be a cats game, player1 should have won" do
      (1..9).to_a.each { |space| player1.take_space(space) }
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect(check_wins.cats_game?(board_snapshot)).to be false
      expect(check_wins.player_win?(board_snapshot, player1)).to be true
      expect(check_wins.player_win?(board_snapshot, player2)).to be false
    end

    it "should be a cats game if no spaces left and no player has won" do
      [1,3,4,6,8].each { |space| player1.take_space(space) }
      [2,5,7,9].each { |space| player2.take_space(space) }
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect(board_snapshot.remaining_spaces).to eq( [] )
      expect(check_wins.cats_game?(board_snapshot)).to be true
      expect(check_wins.player_win?(board_snapshot, player1)).to be false
      expect(check_wins.player_win?(board_snapshot, player2)).to be false
    end
  end

  describe ".game_over?:" do
    it "should not be game over when game starts" do
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect(check_wins.game_over?(board_snapshot)).to be false
    end

    it "should not be game over if player1 has spaces [1,2]" do
      player1.take_space(1)
      player1.take_space(2)
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect(check_wins.game_over?(board_snapshot)).to be false
    end

    it "should be game over if player1 has spaces [1,2,3]" do
      [1,2,3].each { |space| player1.take_space(space) }

      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect(check_wins.game_over?(board_snapshot)).to be true
    end

    it "should be game over if player2 has spaces [9,5,1]" do
      [9,5,1].each { |space| player2.take_space(space) }
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect(check_wins.game_over?(board_snapshot)).to be true
    end

    it "should be game over if no spaces are remaining" do
      [1,3,4,6,8].each { |space| player1.take_space(space) }
      [2,5,7,9].each { |space| player2.take_space(space) }
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect(check_wins.game_over?(board_snapshot)).to be true
    end
  end
end
