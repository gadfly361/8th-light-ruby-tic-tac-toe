require 'ai/minimax_ai'
require 'board'
require 'wins'
require 'check_wins'
require 'board_snapshot'
require 'player/player'
require 'turn'

describe MinimaxAI do
  let(:board) { Board.new(3) }
  let(:player1) { Player.new }
  let(:player2) { Player.new }
  let(:board_snapshot) { BoardSnapshot.new(board, player1, player2) }
  let(:ai) { MinimaxAI.new(board, Wins, CheckWins, Turn, BoardSnapshot) }
  
  describe ".score:" do
    it "should start off with a score of 0" do
      expect( ai.score(board_snapshot, 0) ).to be(0)
      expect( Turn.current_player(board_snapshot) ).to be(player1)
      end
    
    it "should be -100 from player2's perspective" do
      [1,2,3].each { |space| player1.take_space(space) }
      [4,5].each { |space| player2.take_space(space) }
      
      expect( ai.score(board_snapshot,0) ).to be(-100)
      expect( Turn.current_player(board_snapshot) ).to eq(player2)
    end

    it "should be -100 from player1's perspective" do
      [1,2,7].each { |space| player1.take_space(space) }
      [4,5,6].each { |space| player2.take_space(space) }
      
      expect( ai.score(board_snapshot,0) ).to be(-100)
      expect( Turn.current_player(board_snapshot) ).to eq(player1)
    end

    it "should be 0 if game is not over" do
      [1,2,7].each { |space| player1.take_space(space) }
      [4,5,8].each { |space| player2.take_space(space) }
      
      expect( ai.score(board_snapshot, 0) ).to be(0)
      expect( Turn.current_player(board_snapshot) ).to eq(player1)
    end

    it "should be 0 if cats game" do
      [1,2,5,6,7].each { |space| player1.take_space(space) }
      [3,4,8,9].each { |space| player2.take_space(space) }
      
      expect( ai.score(board_snapshot, 0) ).to be(0)
      expect( Turn.current_player(board_snapshot) ).to eq(player2)
    end
  end

  describe ".minimax:" do
    it "should not raise error" do
      [1,2].each { |space| player1.take_space(space) }
      [4,5].each { |space| player2.take_space(space) }

      expect{ ai.minimax(board_snapshot, 0) }.to_not raise_error
    end
    
    it "[99,3]. player1 can win by taking space 3 on next move." do
      [1,2].each { |space| player1.take_space(space) }
      [4,5].each { |space| player2.take_space(space) }

      expect( ai.minimax(board_snapshot, 0) ).to eq([99,3])
    end

    it "[99,5]. player2 can win by taking space 5 on next move." do
      [2,3,4,7].each { |space| player1.take_space(space) }
      [1,8,9].each { |space| player2.take_space(space) }

      expect( ai.minimax(board_snapshot, 0) ).to eq( [99,5] )
      expect( ai.best_space(board_snapshot) ).to eq( 5 )
    end

    it "[0,7]. player2 will draw." do
      [1,5].each { |space| player1.take_space(space) }
      [9].each { |space| player2.take_space(space) }

      expect( ai.minimax(board_snapshot, 0) ).to eq( [0,7] )
      expect( ai.best_space(board_snapshot) ).to eq( 7 )
    end
  end
end
