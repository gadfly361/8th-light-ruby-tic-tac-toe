require 'ai/random_ai'
require 'player/computer_player'
require 'board'
require 'board_snapshot'

describe RandomAI do
  describe ".best_space:" do
    it "should not throw an error when passed board_snapshot" do
      ai = RandomAI.new
      player1 = ComputerPlayer.new(ai)
      player2 = ComputerPlayer.new(ai)
      board = Board.new(3)
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      expect{ player1.pick_space(board_snapshot) }.to_not raise_error
    end
  end
end
