require 'board_snapshot'
require 'board'
require 'player/player'

describe BoardSnapshot do
  let(:board) { Board.new(3) }
  let(:player1) { Player.new }
  let(:player2) { Player.new }

  describe ".remaining_spaces:" do
    it "should have all 9 remaining spaces" do
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      expect(board_snapshot.remaining_spaces).to eq( (1..9).to_a )
    end

    it "should have 2-9 spaces remaining" do
      player1.take_space(1)
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      expect(board_snapshot.remaining_spaces).to eq( (2..9).to_a )
    end

    it "should have 3-9 remaining spaces" do
      player1.take_space(1)
      player2.take_space(2)
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      expect(board_snapshot.remaining_spaces).to eq( (3..9).to_a )
    end
  end

end
