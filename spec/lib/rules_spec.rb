require 'rules'

describe Rules do
  let(:rules) { Rules.new }

  describe ".valid_board_width?:" do
    it "board width should be valid" do
      expect( rules.valid_board_width?(3) ).to be true
    end

    it "board width should be valid" do
      expect( rules.valid_board_width?(4) ).to be true
    end

    it "board width should be valid" do
      expect( rules.valid_board_width?(5) ).to be true
    end

    it "board width should be valid" do
      expect( rules.valid_board_width?(2) ).to be false
    end

    it "board width should be valid" do
      expect( rules.valid_board_width?("3") ).to be false
    end
  end
end
