require 'ui/terminal_ui'
require 'check_wins'
require 'board'
require 'wins'
require 'player/player'
require 'board_snapshot'

describe TerminalUI do
  let(:player1) { Player.new }
  let(:player2) { Player.new }
  let(:board) { Board.new(3) }
  let(:board_snapshot) { BoardSnapshot.new(board, player1, player2) }
  let(:check_wins) { CheckWins.new( Wins ) }

  describe ".board_rows:" do
    it "board_snapshot with player_spaces should exist" do
      expect(board_snapshot.player_spaces(player1)).to eq([])
    end
    
    it "board_snapshot with player_spaces should exist" do
      expect(board_snapshot.player_spaces(player2)).to eq([])
    end
    
    it "board_snapshot with width should exist" do
      expect(board_snapshot.board_width).to eq(3)      
    end

    it "board_snapshot with board_spaces should exist" do
      expect(board_snapshot.board_spaces).to eq( (1..9).to_a )
    end
  end

  describe ".display_if_user_won" do
    it ".player_win? should exist" do
      expect{ check_wins.player_win?(board_snapshot, player1) }.to_not raise_error
    end
  end

  describe ".display_if_opponent_won" do
    it ".player_win? should exist" do
      expect{ check_wins.player_win?(board_snapshot, player2) }.to_not raise_error
    end
  end

  describe ".display_if_cats_game" do
    it ".cats_game? should exist" do
      expect{ check_wins.cats_game?(board_snapshot) }.to_not raise_error
    end
  end
end
