require 'turn'
require 'board'
require 'player/player'
require 'board_snapshot'

describe Turn do
  let(:board) { Board.new(3) }
  let(:player1) { Player.new }
  let(:player2) { Player.new }
  
  describe ".current_player:" do
    it "game should start as player1's turn" do
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect( Turn.current_player(board_snapshot) ).to eq(player1)
      expect( Turn.other_player(board_snapshot) ).to eq(player2)
    end

    it "should be player2 after player1 has gone" do
      player1.take_space(1)
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect( Turn.current_player(board_snapshot) ).to eq(player2)
      expect( Turn.other_player(board_snapshot) ).to eq(player1)
      end

    it "should be player1, after player1 and player2 have both gone" do
      player1.take_space(1)
      player2.take_space(2)
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      
      expect( Turn.current_player(board_snapshot) ).to eq(player1)
      expect( Turn.other_player(board_snapshot) ).to eq(player2)
    end
  end
end
