require 'player/player'

describe Player do
  let(:player) { Player.new }
  
  describe ".spaces:" do
    it "spaces should start with empty array" do
      expect(player.spaces).to eq( [] )
    end
  end

  describe ".take_space:" do
    it "should take 1" do
      player.take_space(1)
      expect( player.spaces ).to eq( [1] )    
    end

    it "should take 1 then 5" do
      player.take_space(1)
      player.take_space(5)
      expect( player.spaces ).to eq( [1, 5] )    
    end
    
    it "should take 5 then 1" do
      player.take_space(5)
      player.take_space(1)
      expect( player.spaces ).to eq( [1, 5] )    
    end

    it "should take 2 then 1 then 9" do
      player.take_space(2)
      player.take_space(1)
      player.take_space(9)
      expect( player.spaces ).to eq( [1, 2, 9] )    
    end
  end
end
