require 'player/computer_player'

describe ComputerPlayer do
  let(:mock_ai) { double }
  let(:computer_player) { ComputerPlayer.new(mock_ai) }
  
  describe ".spaces:" do
    it "spaces should start with empty array" do
      expect(computer_player.spaces).to eq( [] )
    end
  end

  describe ".take_space" do
    it "should take 2 then 1 then 9" do
      computer_player.take_space(2)
      computer_player.take_space(1)
      computer_player.take_space(9)
      expect( computer_player.spaces ).to eq( [1, 2, 9] )    
    end
  end
end
