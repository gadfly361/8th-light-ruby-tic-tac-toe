require 'player/human_player'
require 'board'
require 'board_snapshot'

describe HumanPlayer do  
  let(:mock_ui) { double }
  let(:player1) { HumanPlayer.new(mock_ui) }
  let(:player2) { HumanPlayer.new(mock_ui) }
  let(:board) { Board.new(3) }
  
  describe ".pick_space:" do
    it "should pick space 1" do
      allow(mock_ui).to receive(:ask_for_move)
      allow(mock_ui).to receive(:get_response).and_return(1)

      board_snapshot = BoardSnapshot.new(board, player1, player2)
      move = player1.pick_space( board_snapshot )
      expect(move).to eq(1)
    end

    it "should pick invalid space" do
      allow(mock_ui).to receive(:ask_for_move)
      allow(mock_ui).to receive(:get_response).and_return(10,1)
      allow(mock_ui).to receive(:move_error)

      board_snapshot = BoardSnapshot.new(board, player1, player2)
      player1.pick_space( board_snapshot )
      expect(mock_ui).to have_received(:move_error)
    end
  end

  describe ".spaces:" do
    it "spaces should start with empty array" do
      expect(player1.spaces).to eq( [] )
    end
  end

  describe ".take_space:" do
    it "should take 2 then 1 then 9" do
      player1.take_space(2)
      player1.take_space(1)
      player1.take_space(9)
      expect( player1.spaces ).to eq( [1, 2, 9] )    
    end
  end
end
