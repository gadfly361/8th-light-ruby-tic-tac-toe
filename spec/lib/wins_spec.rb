require 'board'
require 'wins'
require 'set'

describe Wins do
  let(:board3) { Board.new(3) }
  let(:board4) { Board.new(4) }
  let(:board5) { Board.new(5) }
  
  describe ".horizontal_wins:" do
    it "for board of width 3 should have 3 horizantal wins" do
      h_wins = [[1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]]
      expect( Wins.horizantal(board3) ).to eq( h_wins.to_set )
    end

    it "for board of width 4 should have 4 horizantal wins" do
      h_wins = [[1, 2, 3, 4],
                [5, 6, 7, 8],
                [9, 10, 11, 12],
                [13, 14, 15, 16]]
      expect( Wins.horizantal(board4) ).to eq( h_wins.to_set )
    end

    it "for board of width 5 should have 5 horizantal wins" do
      h_wins = [[1, 2, 3, 4, 5],
                [6, 7, 8, 9, 10],
                [11, 12, 13, 14, 15],
                [16, 17, 18, 19, 20],
                [21, 22, 23, 24, 25]]
      expect( Wins.horizantal(board5) ).to eq( h_wins.to_set )
    end
  end

  describe ".vertical_wins:" do
    it "for board of width 3 should have 3 vertical wins" do
      v_wins = [[1, 4, 7],
                [2, 5, 8],
                [3, 6, 9]]
      expect( Wins.vertical(board3) ).to eq( v_wins.to_set )
    end

    it "for board of width 4 should have 4 vertical wins" do
      v_wins = [[1, 5, 9, 13],
                [2, 6, 10, 14],
                [3, 7, 11, 15],
                [4, 8, 12, 16]]
      expect( Wins.vertical(board4) ).to eq( v_wins.to_set )
    end

    it "for board of width 5 should have 5 vertical wins" do
      v_wins = [[1, 6, 11, 16, 21],
                [2, 7, 12, 17, 22],
                [3, 8, 13, 18, 23],
                [4, 9, 14, 19, 24],
                [5, 10, 15, 20, 25]]
      expect( Wins.vertical(board5) ).to eq( v_wins.to_set )
    end
  end

  describe ".diagonal_wins:" do
    it "for board of width 3 should have 2 diagonal wins" do
      d_wins = [[1, 5, 9],
                [3, 5, 7]]
      expect( Wins.diagonal(board3) ).to eq( d_wins.to_set )
    end

    it "for board of width 4 should have 2 diagonal wins" do
      d_wins = [[1, 6, 11, 16],
                [4, 7, 10, 13]]
      expect( Wins.diagonal(board4) ).to eq( d_wins.to_set )
    end

    it "for board of width 5 should have 2 diagonal wins" do
      d_wins = [[1, 7, 13, 19, 25],
                [5, 9, 13, 17, 21]]
      expect( Wins.diagonal(board5) ).to eq( d_wins.to_set )
    end
  end

  describe ".all:" do
    it "for board of width 3 should have 8 wins" do
      all_wins = [[1, 2, 3],
                  [4, 5, 6],
                  [7, 8, 9],
	          
	          [1, 4, 7],
	          [2, 5, 8],
	          [3, 6, 9],

	          [1, 5, 9],
	          [3, 5, 7]]
      expect( Wins.all(board3) ).to eq( all_wins.to_set )
    end

    it "for board of width 4 should have 10 wins" do
      all_wins = [[1, 2, 3, 4],
	          [5, 6, 7, 8],
	          [9, 10, 11, 12],
	          [13, 14, 15, 16],

	          [1, 5, 9, 13],
	          [2, 6, 10, 14],
	          [3, 7, 11, 15],
	          [4, 8, 12, 16],

	          [1, 6, 11, 16],
	          [4, 7, 10, 13]]
      expect( Wins.all(board4) ).to eq( all_wins.to_set )
    end

    it "for board of width 5 should have 12 wins" do
      all_wins = [[1, 2, 3, 4, 5],
                  [6, 7, 8, 9, 10],
                  [11, 12, 13, 14, 15],
                  [16, 17, 18, 19, 20],
                  [21, 22, 23, 24, 25],

                  [1, 6, 11, 16, 21],
                  [2, 7, 12, 17, 22],
                  [3, 8, 13, 18, 23],
                  [4, 9, 14, 19, 24],
                  [5, 10, 15, 20, 25],

                  [1, 7, 13, 19, 25],
                  [5, 9, 13, 17, 21]]
      expect( Wins.all(board5) ).to eq( all_wins.to_set )
    end
  end

  describe ".win?:" do
    it "for board of width 3, player with spaces [1,2,3] should win" do
      expect( Wins.win?(board3, [1,2,3]) ).to be true
    end

    it "for board of width 3, player with spaces [1, 2] should not win" do
      expect( Wins.win?(board3, [1,2]) ).to be false
    end

    it "for board of width 3, player with spaces [1, 2, 3, 4] should win" do
      expect( Wins.win?(board3, [1,2,3,4]) ).to be true
    end

    it "for board of width 4, player with spaces [1,2,3] should not win" do
      expect( Wins.win?(board4, [1,2,3]) ).to be false
    end

    it "for board of width 4, player with spaces [1,2,3,4] should win" do
      expect( Wins.win?(board4, [1,2,3,4]) ).to be true
    end

    it "for board of width 4, player with spaces [1,2,3,6,11,12,16] should win" do
      expect( Wins.win?(board4, [1,2,3,6,11,12,16]) ).to be true
    end

    it "for board of width 4, player with spaces [1,2,3,6,11,12,15] should not win" do
      expect( Wins.win?(board4, [1,2,3,6,11,12,15]) ).to be false
    end
  end
end
