require 'board'

describe Board do
  describe ".spaces:" do
    it "board of width 3 should have 9 spaces" do
      board = Board.new(3)
      expect(board.spaces.count).to eq( 9 )
    end

    it "board of width 4 should have 16 spaces" do
      board = Board.new(4)
      expect(board.spaces.count).to eq( 16 )
    end

    it "board of width 5 should have 25 spaces" do
      board = Board.new(5)
      expect(board.spaces.count).to eq( 25 )
    end
  end

  describe ".rows:" do
    it "board of width 3 should have 3 rows" do
      board = Board.new(3)
      expect(board.rows).to eq([[1, 2, 3],
                                [4, 5, 6],
                                [7, 8, 9]])
    end

    it "board of width 4 should have 4 rows" do
      board = Board.new(4)
      expect(board.rows).to eq([[1, 2, 3, 4],
                                [5, 6, 7, 8],
                                [9, 10, 11, 12],
                                [13, 14, 15, 16]])
    end

    it "board of width 5 should have 5 rows" do
      board = Board.new(5)
      expect(board.rows).to eq([[1, 2, 3, 4, 5],
                                [6, 7, 8, 9, 10],
                                [11, 12, 13, 14, 15],
                                [16, 17, 18, 19, 20],
                                [21, 22, 23, 24, 25]])
    end
  end

  describe ".columns:" do
    it "board of width 3 should have 3 columns" do
      board = Board.new(3)
      expect(board.columns).to eq([[1, 4, 7],
                                   [2, 5, 8],
                                   [3, 6, 9]])
    end

    it "board of width 4 should have 4 columns" do
      board = Board.new(4)
      expect(board.columns).to eq([[1, 5, 9, 13],
                                   [2, 6, 10, 14],
                                   [3, 7, 11, 15],
                                   [4, 8, 12, 16]])
    end

    it "board of width 5 should have 5 columns" do
      board = Board.new(5)
      expect(board.columns).to eq([[1, 6, 11, 16, 21],
                                   [2, 7, 12, 17, 22],
                                   [3, 8, 13, 18, 23],
                                   [4, 9, 14, 19, 24],
                                   [5, 10, 15, 20, 25]])
    end
  end
end
