require 'game'
require 'rules'
require 'board'
require 'wins'
require 'player/player'
require 'player/human_player'
require 'player/computer_player'
require 'ai/random_ai'
require 'ai/minimax_ai'
require 'setup_game'
require 'turn'
require 'board_snapshot'
require 'check_wins'

describe Game do
  let(:mock_ui) { double }
  let(:game) { Game.new(ui: mock_ui, rules: Rules.new, board_class: Board,
                        wins_class: Wins, human_player_class: HumanPlayer,
                        computer_player_class: ComputerPlayer, random_ai: RandomAI.new,
                        minimax_ai_class: MinimaxAI,
                        setup_game_class: SetupGame, turn_class: Turn,
                        board_snapshot_class: BoardSnapshot,
                        check_wins_class: CheckWins) }

  describe ".setup_game:" do
    it "should not throw an error if board size of 3 and playing against computer" do
      allow(mock_ui).to receive(:ask_for_board_width)
      allow(mock_ui).to receive(:ask_if_computer_opponent)
      allow(mock_ui).to receive(:get_response).and_return("3", "y")
      expect{ game.setup_game }.to_not raise_error
    end

    it "should not throw an error if board size of 3 and playing against human" do
      allow(mock_ui).to receive(:ask_for_board_width)
      allow(mock_ui).to receive(:ask_if_computer_opponent)
      allow(mock_ui).to receive(:get_response).and_return("3", "n")
      expect{ game.setup_game }.to_not raise_error
    end
  end
  
  describe ".move:" do
    let(:player1) { HumanPlayer.new(mock_ui) }
    let(:player2) { HumanPlayer.new(mock_ui) }
    let(:board) { Board.new(3) }
    
    it "user should take space 1" do
      allow(mock_ui).to receive(:ask_for_move)
      allow(mock_ui).to receive(:get_response).and_return("1")
      
      board_snapshot = BoardSnapshot.new(board, player1, player2)
      game.move(player1, board_snapshot)
      expect(player1.spaces).to eq([1])
    end

    it "user takes spaces 1, then player2 tries to take space 1, fails, and then takes space 2" do
      allow(mock_ui).to receive(:ask_for_move)
      allow(mock_ui).to receive(:get_response).and_return("1", "1", "2")
      allow(mock_ui).to receive(:move_error)

      board_snapshot = BoardSnapshot.new(board, player1, player2)      
      game.move(player1, board_snapshot)
      game.move(player2, board_snapshot)
      expect(player1.spaces).to eq([1])
      expect(player2.spaces).to eq([2])
      expect(mock_ui).to have_received(:move_error)
    end
  end

  describe ".play_again" do
    it "should be invalid response to play again" do
      allow(mock_ui).to receive(:ask_to_play_again)
      allow(mock_ui).to receive(:get_response).and_return("invalid","n")
      allow(mock_ui).to receive(:play_again_error)
      game.play_again
      expect(mock_ui).to have_received(:play_again_error)
    end
  end
end
