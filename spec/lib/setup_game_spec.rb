require 'setup_game'
require 'rules'
require 'board'
require 'player/player'
require 'player/human_player'
require 'player/computer_player'
require 'ai/random_ai'
require 'ai/minimax_ai'
require 'board_snapshot'
require 'wins'
require 'check_wins'
require 'turn'

describe SetupGame do
  let(:mock_ui) { double }
  let(:setup_game) { SetupGame.new(ui: mock_ui, rules: Rules.new, board_class: Board,
                                   human_player_class: HumanPlayer, computer_player_class: ComputerPlayer,
                                   random_ai: RandomAI.new, minimax_ai_class: MinimaxAI,
                                   board_snapshot_class: BoardSnapshot, wins_class: Wins,
                                   check_wins_class: CheckWins, turn_class: Turn) }

  describe ".create_board_width:" do
    it "board width to be 3" do
      allow(mock_ui).to receive(:ask_for_board_width)
      allow(mock_ui).to receive(:get_response).and_return("3")
      bw = setup_game.create_board_width
      expect(bw).to eq(3)
    end

      it "board width to be 4" do
        allow(mock_ui).to receive(:ask_for_board_width)
        allow(mock_ui).to receive(:get_response).and_return("4")
        bw = setup_game.create_board_width
        expect(bw).to eq(4)
      end

      it "board width of 6 to throw an error" do
        allow(mock_ui).to receive(:ask_for_board_width)
        allow(mock_ui).to receive(:get_response).and_return("6","5")
        allow(mock_ui).to receive(:board_width_error)
        setup_game.create_board_width
        expect(mock_ui).to have_received(:board_width_error)
      end
    end

    describe ".create_board:" do
      it "should create board of width 3" do
        allow(mock_ui).to receive(:ask_for_board_width)
        allow(mock_ui).to receive(:get_response).and_return("3")

        board = setup_game.create_board
        expect(board.width).to eq(3)
      end
    end

    describe ".create players:" do

      let(:board) { Board.new(3) }
      
      it "should create a computer opponent" do
        allow(mock_ui).to receive(:ask_if_computer_opponent)
        allow(mock_ui).to receive(:get_response).and_return("y")
        
        expect(setup_game.create_opponent(board).class.name).to eq("ComputerPlayer")
      end

      it "should create a human opponent" do
        allow(mock_ui).to receive(:ask_if_computer_opponent)
        allow(mock_ui).to receive(:get_response).and_return("n")
        expect(setup_game.create_opponent(board).class.name).to eq("HumanPlayer")
      end

      it "user should provide invalid response to create opponent" do
        allow(mock_ui).to receive(:ask_if_computer_opponent)
        allow(mock_ui).to receive(:get_response).and_return("invalid","y")
        allow(mock_ui).to receive(:computer_opponent_error)
        setup_game.create_opponent(board)
        expect(mock_ui).to have_received(:computer_opponent_error)
      end
    
      it "should create a human player1 and a computer player2" do
        allow(mock_ui).to receive(:ask_if_computer_opponent)
        allow(mock_ui).to receive(:get_response).and_return("y")
        expect(setup_game.create_user.class.name).to eq("HumanPlayer")
        expect(setup_game.create_opponent(board).class.name).to eq("ComputerPlayer")
      end

      it "should create a human player1 and a human player2" do
        allow(mock_ui).to receive(:ask_if_computer_opponent)
        allow(mock_ui).to receive(:get_response).and_return("n")
        expect(setup_game.create_user.class.name).to eq("HumanPlayer")
        expect(setup_game.create_opponent(board).class.name).to eq("HumanPlayer")    
      end

      it "should create a human player1 and a human player2 (after an invalid initial response)" do
        allow(mock_ui).to receive(:ask_if_computer_opponent)
        allow(mock_ui).to receive(:get_response).and_return("invalid","n")
        allow(mock_ui).to receive(:computer_opponent_error)
        expect(setup_game.create_user.class.name).to eq("HumanPlayer")
        expect(setup_game.create_opponent(board).class.name).to eq("HumanPlayer")
        expect(mock_ui).to have_received(:computer_opponent_error)
      end
  end
end
