require './lib/game.rb'
require './lib/ui/terminal_ui.rb'
require './lib/rules.rb'
require './lib/board.rb'
require './lib/wins.rb'
require './lib/player/player.rb'
require './lib/player/human_player.rb'
require './lib/player/computer_player.rb'
require './lib/ai/random_ai.rb'
require './lib/setup_game.rb'
require './lib/turn.rb'
require './lib/board_snapshot.rb'
require './lib/check_wins.rb'
require './lib/ai/minimax_ai.rb'

game = Game.new(ui: TerminalUI.new, rules: Rules.new, board_class: Board,
                wins_class: Wins, human_player_class: HumanPlayer,
                computer_player_class: ComputerPlayer, random_ai: RandomAI.new,
                setup_game_class: SetupGame, turn_class: Turn,
                board_snapshot_class: BoardSnapshot, check_wins_class: CheckWins,
                minimax_ai_class: MinimaxAI)
game.play
