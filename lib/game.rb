class Game  
  def initialize(ui:, rules:, board_class:, wins_class:,
                 human_player_class:, computer_player_class:,
                 random_ai:, minimax_ai_class:,
                 setup_game_class:, turn_class:,
                 board_snapshot_class:, check_wins_class:)
    @ui = ui
    @rules = rules
    @board_class = board_class
    @wins_class = wins_class
    @human_player_class = human_player_class
    @computer_player_class = computer_player_class
    @random_ai = random_ai
    @setup_game_class = setup_game_class
    @turn_class = turn_class
    @board_snapshot_class = board_snapshot_class
    @check_wins_class = check_wins_class
    @minimax_ai_class = minimax_ai_class
  end

  def play
    board_snapshot = setup_game
    board_width = board_snapshot.board_width
    board = @board_class.new(board_width)
    check_wins = @check_wins_class.new(@wins_class)
    turn_sequence(board_snapshot, check_wins)
    end_game_results(board_snapshot, check_wins)
    play_again
  end

  def setup_game
    setup_game = @setup_game_class.new(ui: @ui, rules: @rules, board_class: @board_class,
                                       human_player_class: @human_player_class,
                                       computer_player_class: @computer_player_class,
                                       random_ai: @random_ai, minimax_ai_class: @minimax_ai_class,
                                       board_snapshot_class: @board_snapshot_class,
                                       wins_class: @wins_class, check_wins_class: @check_wins_class,
                                       turn_class: @turn_class)
    setup_game.create_board_snapshot
  end

  def turn_sequence(board_snapshot, check_wins)
    until check_wins.game_over?(board_snapshot)
      player = @turn_class.current_player(board_snapshot)
      display_board(board_snapshot)
      move( player, board_snapshot)
    end
  end

  def display_board(board_snapshot)
    @ui.display_board(board_snapshot)
  end

  def move(player, board_snapshot)
    player.take_space( player.pick_space(board_snapshot) )
  end

  def end_game_results(board_snapshot, check_wins)
    @ui.display_results(board_snapshot, check_wins)
  end

  def play_again
    @ui.ask_to_play_again
    response = @ui.get_response
    if response == "y"
      play
    elsif response == "n"
      nil
    else
      @ui.play_again_error
      play_again
    end
  end
end
