class Player
  def initialize
    @spaces = Array.new
  end

  def take_space(space)
    @spaces << space
  end

  def spaces
    @spaces = @spaces.sort
  end
end
