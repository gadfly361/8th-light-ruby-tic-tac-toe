require_relative './player'

class ComputerPlayer < Player
  def initialize(ai)
    @ai = ai
    @spaces = Array.new
  end

  def pick_space(board_snapshot)
    @ai.best_space(board_snapshot)
  end
end
