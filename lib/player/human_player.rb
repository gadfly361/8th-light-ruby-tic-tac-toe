require_relative './player'

class HumanPlayer < Player
  def initialize(ui)
    @ui = ui
    @spaces = Array.new
  end

  def pick_space(board_snapshot)
    remaining_spaces = board_snapshot.remaining_spaces
    @ui.ask_for_move(board_snapshot.board_width)
    move = @ui.get_response.to_i
    if remaining_spaces.include?(move)
      move
    else
      @ui.move_error
      pick_space(board_snapshot)
    end
  end
end
