class Board
  attr_reader :width

  def initialize(width)
    @width = width
  end
  
  def spaces
    number_of_spaces = @width * @width
    (1..number_of_spaces).to_a
  end

  def rows
    spaces.each_slice(@width).to_a
  end

  def columns
    (1..@width).map do |n|
      rows.map { |row| row[n-1] }
    end
  end
end
