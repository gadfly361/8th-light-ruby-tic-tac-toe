require 'set'

class BoardSnapshot

  attr_reader :board, :player1, :player2
  
  def initialize(board, player1, player2)
    @board = board
    @player1 = player1
    @player2 = player2
  end

  def board_width
    @board.width
  end

  def board_spaces
    @board.spaces
  end

  def player_spaces(player)
    player.spaces
  end
  
  def remaining_spaces
    board_spaces = @board.spaces.to_set
    player1_spaces = player_spaces(@player1).to_set
    player2_spaces = player_spaces(@player2).to_set

    remaining_spaces = board_spaces - player1_spaces - player2_spaces
    remaining_spaces.to_a
  end
end
