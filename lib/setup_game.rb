class SetupGame

  def initialize(ui:, rules:, board_class:, human_player_class:, computer_player_class:,
                 random_ai:, minimax_ai_class:, board_snapshot_class:,
                 wins_class:, check_wins_class:, turn_class:)
    @ui = ui
    @rules = rules
    @board_class = board_class
    @human_player_class = human_player_class
    @computer_player_class = computer_player_class
    @random_ai = random_ai
    @minimax_ai_class = minimax_ai_class
    @board_snapshot_class = board_snapshot_class
    @wins_class = wins_class
    @check_wins_class = check_wins_class
    @turn_class = turn_class
  end
  
  def create_board_width
    @ui.ask_for_board_width
    board_width = @ui.get_response.to_i
    if @rules.valid_board_width?(board_width)
      board_width
    else
      @ui.board_width_error
      create_board_width
    end
  end

  def create_board
    bw = create_board_width
    @board = @board_class.new(bw)
  end
  
  def create_opponent(board)
    @ui.ask_if_computer_opponent
    response = @ui.get_response
    if response == "y" && board.width == 3
      create_hard_computer_player
    elsif response == "y"
      create_easy_computer_player
    elsif response == "n"
      create_human_player
    else
      @ui.computer_opponent_error
      create_opponent(board)
    end
  end

  def create_user
    @human_player_class.new(@ui)
  end

  def create_board_snapshot
    board = create_board
    player1 = create_user
    player2 = create_opponent(board)
    @board_snapshot_class.new(board, player1, player2)
  end

  private

  def create_hard_computer_player
    minimax_ai = @minimax_ai_class.new(@board, @wins_class, @check_wins_class, @turn_class, @board_snapshot_class)
    @computer_player_class.new(minimax_ai)
  end

  def create_easy_computer_player
    @computer_player_class.new(@random_ai)
  end

  def create_human_player
    @human_player_class.new(@ui)
  end
end
