class MinimaxAI

  def initialize(board, wins_class, check_wins_class, turn_class, board_snapshot_class)
    @check_wins = check_wins_class.new(wins_class)
    @turn_class = turn_class
    @wins_class = wins_class
    @board_snapshot_class = board_snapshot_class
  end

  def best_space(board_snapshot)
    best_score_and_move = minimax(board_snapshot, 0)
    best_move = best_score_and_move[1]
  end
  
  def minimax(board_snapshot, depth)
    if @check_wins.game_over?(board_snapshot)
      terminal_node_score_and_move(board_snapshot, depth)
    elsif is_first_round?(board_snapshot)
      minimax_first_move(board_snapshot)
    else
      scores_and_moves = get_scores_and_moves(board_snapshot, depth)
      find_best_score_and_move(scores_and_moves)
    end
  end

  def score(board_snapshot, depth)
    board = board_snapshot.board
    player = @turn_class.current_player(board_snapshot)
    opponent = @turn_class.other_player(board_snapshot)
    @wins_class.win?(board, opponent.spaces) ? depth - 100 : 0
  end

  private

  def terminal_node_score_and_move(board_snapshot, depth)
    terminal_node = []
    terminal_node << score(board_snapshot, depth)
    terminal_node << "dummy_move"
  end

  def is_first_round?(board_snapshot)
    board_width = board_snapshot.board_width
    total_board_spaces_count = board_width * board_width
    remaining_spaces_count =  board_snapshot.remaining_spaces.count

    total_board_spaces_count - remaining_spaces_count <= 2
  end
  
  def minimax_first_move(board_snapshot)
    response = { 1 => 5, 2 => 8, 3 => 5,
                 4 => 7, 5 => 9, 6 => 9,
                 7 => 5, 8 => 9, 9 => 5 }
    
    player = @turn_class.current_player(board_snapshot)
    opponent = @turn_class.other_player(board_snapshot)
    opponent_move = opponent.spaces[0]
    response_move = response[opponent_move]
    
    score_and_move = ["dummy score", response_move]
  end

  def get_scores_and_moves(board_snapshot, depth)
    board_snapshot.remaining_spaces.map do |move|
      new_board_snapshot = copy_board_snapshot(board_snapshot)
      @turn_class.current_player(new_board_snapshot).take_space(move)
      create_score_and_move(new_board_snapshot, depth, move)
    end
  end

  def create_score_and_move(board_snapshot, depth, move)
    depth += 1
    score_move = []
    score_move << minimax(board_snapshot, depth).first * -1
    score_move << move
  end

  def find_best_score_and_move(scores_and_moves)
    scores_and_moves.sort.reverse.first
  end

  def copy_board_snapshot(board_snapshot)
    board = board_snapshot.board.clone
    player1 = board_snapshot.player1.clone
    player2 = board_snapshot.player2.clone
    @board_snapshot_class.new(board,player1,player2)
  end
end
