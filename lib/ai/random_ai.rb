class RandomAI
  def best_space(board_snapshot)
    remaining_spaces = board_snapshot.remaining_spaces
    remaining_spaces.sample
  end
end
