class CheckWins
  def initialize(wins_class)
    @wins_class = wins_class
  end

  def player_win?(board_snapshot, player)
    board = board_snapshot.board
    player_spaces = board_snapshot.player_spaces(player)
    @wins_class.win?(board, player_spaces)
  end

  def cats_game?(board_snapshot)
    player1 = board_snapshot.player1
    player2 = board_snapshot.player2
    player1_win = player_win?(board_snapshot, player1)
    player2_win = player_win?(board_snapshot, player2)
    board_snapshot.remaining_spaces == [] && !player1_win && !player2_win
  end
  
  def game_over?(board_snapshot)
    player1 = board_snapshot.player1
    player2 = board_snapshot.player2
    player1_win = player_win?(board_snapshot, player1)
    player2_win = player_win?(board_snapshot, player2)
    cats_game = cats_game?(board_snapshot)
    player1_win || player2_win || cats_game
  end
end
