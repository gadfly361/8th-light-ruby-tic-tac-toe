require 'set'

class Wins  
  def self.horizantal(board)
    board.rows.to_set
  end

  def self.vertical(board)
    board.columns.to_set
  end

  def self.diagonal(board)
    rows = board.rows
    rows_reverse = board.rows.map { |a| a.reverse }
    
    left_to_right = (0...board.width).collect { |i| rows[i][i] }
    right_to_left = (0...board.width).collect { |i| rows_reverse[i][i] }

    diagonals = Array.new
    diagonals << left_to_right
    diagonals << right_to_left
    diagonals.to_set
  end

  def self.all(board)
    horizantal(board) | vertical(board) | diagonal(board)
  end

  def self.win?(board, spaces)
    spaces_set = spaces.to_set
    results = all(board).map { |win_scenario| spaces_set >= win_scenario.to_set }
    results.any?
  end
end
