class TerminalUI
  def line_break
    line_break = "------------------------------\n"
  end

  def print_message(message)
    puts  "\n" + message + "\n"
  end

  def print_error(error)
    puts  line_break + error + "\n" + line_break
  end

  def get_response
    gets.chomp
  end
  
  def ask_for_move(board_width)
    spaces = board_width * board_width
    print_message("Pick a space. (Spaces are numbered 1 through #{spaces})")
  end

  def move_error
    print_error("Oops, that isn't a valid move")
  end  

  
  def ask_for_board_width
    print_message("Choose a board width (3, 4, or 5)")
  end

  def board_width_error
    print_error("Oops, that isn't a valid board width")
  end

  
  def ask_if_computer_opponent
    print_message("Would you like to play against a computer? (y/n)")
  end

  def computer_opponent_error
    print_error("Oops, that isn't a valid answer")
  end

  
  def ask_to_play_again
    print_message("Would you like to play again? (y/n)")
  end

  def play_again_error
    print_error("Oops, that isn't a valid reponse")
  end

  
  def board_rows(board_snapshot)
    player1 = board_snapshot.player1
    player2 = board_snapshot.player2
    
    spaces = Array.new
    board_snapshot.board_spaces.each { |s| spaces << " " }
    board_snapshot.player_spaces(player1).each { |n| spaces[n-1] = "X" }
    board_snapshot.player_spaces(player2).each { |n| spaces[n-1] = "O" }
    rows = spaces.each_slice(board_snapshot.board_width).to_a
  end

  def display_board(board_snapshot)
    display = String.new
    board_rows(board_snapshot).each do
      |row| display << "| " + row.join(" | ") + " |\n"
    end
    puts line_break + display
  end

  
  def display_if_user_won(board_snapshot, check_wins)
    player1 = board_snapshot.player1
    print_message("You won!") if check_wins.player_win?(board_snapshot, player1)
  end

  def display_if_opponent_won(board_snapshot, check_wins)
    player2 = board_snapshot.player2
    print_message("Aww, you lost!") if check_wins.player_win?(board_snapshot, player2)
  end

  def display_if_cats_game(board_snapshot, check_wins)
    print_message("Cats game!") if check_wins.cats_game?(board_snapshot)
  end
  
  def display_results(board_snapshot, check_wins)    
    display_board(board_snapshot)
    display_if_user_won(board_snapshot, check_wins)
    display_if_opponent_won(board_snapshot, check_wins)
    display_if_cats_game(board_snapshot, check_wins)
  end
end
