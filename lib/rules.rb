class Rules
  def valid_board_width?(board_width)
    [3,4,5].include?(board_width)
  end
end
