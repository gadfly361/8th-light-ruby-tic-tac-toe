class Turn
  def self.current_player(board_snapshot)
    player1 = board_snapshot.player1
    player2 = board_snapshot.player2
    player_space_counts_equal?(board_snapshot) ? player1 : player2
  end

  def self.other_player(board_snapshot)
    player1 = board_snapshot.player1
    player2 = board_snapshot.player2    
    player_space_counts_equal?(board_snapshot) ? player2 : player1
  end

  private

  def self.player_space_counts_equal?(board_snapshot)
    player1 = board_snapshot.player1
    player2 = board_snapshot.player2
    player1_spaces = board_snapshot.player_spaces(player1)
    player2_spaces = board_snapshot.player_spaces(player2)
    player1_spaces.count == player2_spaces.count
  end
end
