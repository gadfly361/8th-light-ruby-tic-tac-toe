# Ruby Tic Tac Toe

My glorious attempt to make a tic tac toe game with ruby.

## Play Game

Type the following in a terminal to run the game:

```
$ ruby PLAY_GAME.rb
```

## Run Tests

Type the following in a terminal to run the tests:

```
$ rspec
```

Or to auto-run the tests, type:

```
$ guard
```


